/*
touches need to:
have current touch,
have previous touch
have touch difference
*/

function SingleTouch(x,y,id){
	this.current = new LV2(x,y);
	this.id = id;
	this.ticks = 0;
	this.previous = this.current.copy();
	this.x = x, this.y = y, this.id = id;
}
SingleTouch.prototype.toString = function(){
	return this.id + ' = ' + this.current;
};
SingleTouch.prototype.isOlder = function(t2){
	if(this.ticks < t2.ticks) return true;
	if(this.ticks == t2.ticks && this.id < t2.id) return true;
	return false;
};

SingleTouch.prototype.update = function(x, y){
	this.previous = this.current.copy();
	this.current = new LV2(x,y);
	this.ticks++;
};

SingleTouch.prototype.dif = function(scalar){
	if(this.previous.x == this.current.x && this.previous.y == this.current.y) return new LV2(0,0);
	var dxy = this.current.sub(this.previous);
	if(scalar === undefined) return dxy;
	var mag = dxy.mag();
	dxy.idiv(mag);
	dxy.iscale(scalar);
	return dxy;
};

SingleTouch.prototype.eq = function(t2){
	return this.id == t2.id;
};

function LTouchHandler(element, tmove){
	this.element = element;
	this.l = [];
	this.tmove = tmove;
	this.element.addEventListener("touchstart", this.handler('touchstart'), false);
	this.element.addEventListener("touchmove", this.handler('touchmove'), false);
	this.element.addEventListener("touchend", this.handler('touchend'), false);
}

LTouchHandler.dif = function(current, previous, scalar){
	if(previous.x == current.x && previous.y == current.y) return new LV2(0,0);
	var dxy = current.sub(previous);
	if(scalar === undefined) return dxy;
	var mag = dxy.mag();
	dxy.idiv(mag);
	dxy.iscale(scalar);
	return dxy;
};


LTouchHandler.prototype.cvtToScreenLocation = function(p, canvasWidth, canvasHeight, screenWidth, screenHeight){
	var rv = p.copy();
	rv.x = (rv.x / screenWidth) * canvasWidth;
	rv.y = (rv.y / screenHeight) * canvasHeight;
	return rv;
};

LTouchHandler.prototype.handler = function(type){
	var me = this;
	if(type == 'touchstart')
		return function(input){
			if(navigator.userAgent.match(/Android/i) || input.preventDefault !== undefined){
				input.preventDefault();
			}
			var touches = input.touches;
			var rv = [];
			for(var i = 0; i < touches.length; i++){
				var t = touches[i];
				rv.push({
					x : t.clientX,
					y : t.clientY,
					id : t.identifier
				});
			}
			me[type](rv);
		};
	return function(input){
			if(navigator.userAgent.match(/Android/i) || input.preventDefault !== undefined){
				input.preventDefault();
			}
			var touches = input.changedTouches;
			var rv = [];
			for(var i = 0; i < touches.length; i++){
				var t = touches[i];
				rv.push({
					x : t.clientX,
					y : t.clientY,
					id : t.identifier
				});
			}
			me[type](rv);
		};
};

LTouchHandler.prototype.getExisting = function(id){
	for(var i = 0; i < this.l.length; i++){
		if(this.l[i].id == id) return this.l[i];
	}
	return undefined;
};

LTouchHandler.prototype.oldestTouch = function(){
	if(this.l.length == 0) return undefined;
	var oldest = this.l[0];
	for(var i = 1; i < this.l.length; i++){
		if(this.l[i].isOlder(oldest)) oldest = this.l[i];
	}
	return oldest;
};

LTouchHandler.prototype.touchstart = function(touches){
	for(var i = 0; i < touches.length; i++){
		var t = touches[i];
		this.l.push(new SingleTouch(t.x, t.y, t.id));
	}
	if(this.tmove !== undefined){
		this.tmove(this.oldestTouch(), 'start');
	}
};
LTouchHandler.prototype.touchmove = function(touches){
	for(var i = 0; i < touches.length; i++){
		var t = touches[i];
		var ex = this.getExisting(t.id);
		if(ex !== undefined){
			ex.update(t.x, t.y);
		}
	}	
	if(this.tmove !== undefined){
		this.tmove(this.oldestTouch(), 'move');
	}
};

LTouchHandler.prototype.touchend = function(touches){
	var rv = [];
	for(var j = 0; j < this.l.length; j++){
		var ta = this.l[j];
		var found = false;	
		for(var i = 0; i < touches.length; i++){
			var tb = touches[i];
			if(tb.id == ta.id){
				found = true;
				break;
			}
		}
		if(!found) rv.push(ta);
		else if(this.tmove !== undefined) this.tmove(ta, 'end');
	}
	this.l = rv;
};

