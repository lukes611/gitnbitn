/*
Game object:
	state:
		0 -> title screen
		1 -> main game
		2 -> tutorial
		3 -> pause screen
		4 -> scores screen
		5 -> transition state
		6 -> finished game state

*/

function Game(){
	this.db = new DBObject();
	this.sound = new Sonic('damage2', 'music');
	this.sound.sounds.damage2.setVolume(0.6);
	this.sound.sounds.music.setVolume(0.4);
	
	this.fpsCount = 0;
	this.fps = 0;
	this.fpsTime = (new Date()).getTime();
	this.drawBoolean = true;
	
	this.gui = new GameGui();
	this.windowHandler = new WindowHandler(this.gui.canvas);
	this.spatialSupervisor = new SpatialSupervisor(this.gui.w, this.gui.h, this.gui.radius);
	this.transitioner = new Transition(this.spatialSupervisor);
	this.starController = new StarController(this.spatialSupervisor);
	this.player = new Player(this.spatialSupervisor);
	this.bController = new BarrierController(this.spatialSupervisor, this.player, 200);
	this.currentTouch = new LV2(-1,-1);
	this.paint = this.draw.bind(this);
	this.state = 0;
	this.soundOn = true;
	this.previousState = 0;
	this.score = 0;

	this.time = 0;
	this.level = 0;
	this.maxHealth = 375;
	this.health = this.maxHealth;
	this.ticksBeforeMoreChallenging = 300;
	this.numberOfLevelSteps = 16;
	this.challengeTime = 0;
	this.messages = [];
	this.outBoundMessages = [];
}

Game.prototype.loadScores = function(){
	for(var i = 0; i < this.db.scores.length && i < 5; i++)
		this.gui.yourScoresLabels[i].text = this.db.scores[i].name + ': ' + this.db.scores[i].score;
};

Game.prototype.addMessage = function(){
	for(var i = 0; i < arguments.length; i++) this.messages.push(new LButton(arguments[i], this.gui.kbFontSize * 0.5, this.spatialSupervisor.width / 2, 0));
};
Game.prototype.stepMessages = function(){
	if(this.messages.length > 0){
		this.messages[0].y+=2;
		if(this.messages[0].y >= this.spatialSupervisor.height / 2) this.outBoundMessages.push(this.messages.shift());
	}
	var narr = [];
	for(var i = 0; i < this.outBoundMessages.length; i++){
		if(this.outBoundMessages[i].y < this.spatialSupervisor.height){
			this.outBoundMessages[i].y += 2;
			narr.push(this.outBoundMessages[i]);
		}
	}
	this.outBoundMessages = narr;
};

Game.prototype.drawMessages = function(){
	if(this.messages.length > 0)
		this.gui.drawButton(this.messages[0]);
	for(var i = 0; i < this.outBoundMessages.length; i++)
		this.gui.drawButton(this.outBoundMessages[i]);
};


Game.prototype.resetMessages = function(){
	this.messages = [];
	this.outBoundMessages = [];
};

Game.prototype.toggleSound = function(){
	this.soundOn = !this.soundOn;
	this.gui.soundToggle.text = 'sound ' + (!this.soundOn ? 'on' : 'off');
	if(!this.soundOn)
		this.sound.muteAll();
	else{
		
		this.sound.unmuteAll();
	}
};

Game.prototype.reset = function(){
	this.state = 0;
	this.drawBoolean = true;
};

Game.prototype.init = function(){
	
	this.gui.insert();
	this.windowHandler.update();
	this.windowHandler.repaint(this.paint);
	this.touchHandler = new LTouchHandler(window, this.touch.bind(this));
	if(this.soundOn) this.sound.sounds.music.play();
};

Game.prototype.mainGameInit = function(){
	this.resetMessages();
	this.bController.speed = 200;
	this.bController.yspeed = 2;
	this.bController.extraSpace = 1.0;
	this.bController.reset();
	this.player.setN(1);
	this.time = 0;
	this.level = 0;
	this.score = 0;
	this.challengeTime = 0;
	this.health = this.maxHealth;
};

Game.prototype.tutorialInit = function(){
	this.resetMessages();
	this.bController.speed = 200;
	this.bController.yspeed = 2;
	this.bController.extraSpace = 1.0;
	this.bController.reset();
	this.bController.time = 0;
	this.player.setN(1);
	this.time = 0;
	this.bController.time = -1500;
	this.challengeTime = 0;
	this.addMessage('ok.. so you are that disc', 'you must avoid barriers');
	this.addMessage('touch the screen', 'move your finger', 'feel that movement?');
	this.addMessage('that is spatial movement');
	this.addMessage('increasing the the discs');
};

Game.prototype.incChallenge = function(){
	if(this.challengeTime < this.numberOfLevelSteps-1){
		this.bController.speed -= 10;
	}else if(this.challengeTime == this.numberOfLevelSteps-1 && this.player.N < 4){
		this.bController.speed = 200;
		this.bController.incrementN();
		this.addMessage('level ' + ++this.level + ' complete');
	}else{
		this.bController.extraSpace -= 0.1;
		this.bController.setN(1);
		this.bController.speed = 200;
		this.addMessage('level ' + ++this.level + ' complete');
	}
	this.challengeTime = (this.challengeTime + 1) % this.numberOfLevelSteps;
};


Game.prototype.draw = function(){
	//this.fpsCount++;
	//var currentTime = (new Date()).getTime();
	//var diff = currentTime - this.fpsTime;
	//if(diff > 200){
	//	this.fps = this.fpsCount / (diff/1000);
	//	this.fpsTime = currentTime;
	//	this.fpsCount = 0;
	//}
	if(this.transitioner.state != 0){
		this.gui.clear();
		this.gui.drawStars(this.starController);
	}
	

	if(this.state == 0) //draw title screen
		this.drawTitleScreen();
	else if(this.state == 1) //draw game
		this.drawGameScreen();
	else if(this.state == 2) //draw tutorial screen
		this.drawTutorialScreen();
	else if(this.state == 3) //draw pause screen
		this.drawPauseScreen();
	else if(this.state == 4) //draw scores screen
		this.drawScoresScreen();
	else if(this.state == 6) //draw playerIsDead screen
		this.drawGameOverScreen();
	
	if(this.transitioner.state != -1){
		this.drawTransitionScreen();
	}
	

	//this.drawTest();
	if(this.drawBoolean) this.windowHandler.repaint(this.paint);
};

Game.prototype.drawScoresScreen = function(){
	this.gui.drawButton(this.gui.scoresLabel);
	for(var i = 0; i < 5; i++){ 
		this.gui.drawButton(this.gui.yourScoresLabels[i]);
		//this.gui.drawButton(this.gui.worldScoresLabels[i]);
	}
	this.gui.drawButton(this.gui.worldScoresLabels[0]);
	this.gui.drawButton(this.gui.scoresLabel2);
	this.gui.drawButton(this.gui.returnButton);
};

Game.prototype.drawGameOverScreen = function(){
	this.gui.finalScoreOut.text = this.score+'';
	this.gui.drawButton(this.gui.finalScore);
	this.gui.drawButton(this.gui.finalScoreOut);
	this.gui.drawButton(this.gui.returnButton);
	this.gui.drawButton(this.gui.nameEnterButton);
	this.gui.drawButton(this.gui.spaceButton);
	this.gui.drawButton(this.gui.goButton);
	this.gui.drawButton(this.gui.backSpaceButton);
	for(var i = 0; i < this.gui.keyBoard.length; i++)
		this.gui.drawButton(this.gui.keyBoard[i]);
};

Game.prototype.drawTransitionScreen = function(){
	for(var i = 0; i < this.transitioner.numBars; i++){
		this.gui.canvas.setFill(this.transitioner.colors[i]);
		this.gui.canvas.rectf(0, this.transitioner.yaxis(i), this.transitioner.width(i), this.transitioner.height);
	}
};

Game.prototype.drawTitleScreen = function(){
	this.gui.drawTitle('pluto');
	this.gui.drawButton(this.gui.playButton);
	this.gui.drawButton(this.gui.tutorialButton);
	this.gui.drawButton(this.gui.soundToggle);
	
	this.gui.drawButton(this.gui.scoresButton);
	this.gui.drawButton(this.gui.exitButton);
};

Game.prototype.drawGameScreen = function(){
	//this.gui.drawTitle('pluto');
	this.drawMessages();
	this.gui.drawButton(this.gui.pauseButton);
	this.gui.drawPlayer(this.player, this.wasHit);
	this.gui.drawBarriers(this.bController);
	this.gui.drawHealth(this.health / this.maxHealth);
	this.gui.playerScoreLabel.text = 'score: ' + this.score;
	//this.gui.playerScoreLabel.text = Math.round(this.fps);
	this.gui.drawButton(this.gui.playerScoreLabel);

};

Game.prototype.drawTutorialScreen = function(){
	this.drawMessages();
	this.gui.drawButton(this.gui.returnButton);
	this.gui.drawPlayer(this.player, this.wasHit);
	this.gui.drawBarriers(this.bController);
	this.gui.drawHealth(this.health / this.maxHealth);
};

Game.prototype.drawPauseScreen = function(){
	this.drawGameScreen();
	this.gui.overlay();
	this.gui.drawButton(this.gui.resumeButton);
	this.gui.drawButton(this.gui.tutorialButton);
	this.gui.drawButton(this.gui.soundToggle);
	this.gui.drawButton(this.gui.scoresButton);
	this.gui.drawButton(this.gui.exitButton);
};

Game.prototype.step = function(){
	if(this.transitioner.state != -1){
		if(this.transitioner.step())
			this.state = this.transitioner.nextState;
	}
	this.starController.step();
	if(this.state == 1){ //main game
		this.wasHit = this.bController.colission();
		if(this.wasHit){
			this.health -= 2;
			if(this.health <= 0){
				this.sound.sounds.damage2.stop();
				this.stateChange(6);
				return;
			}
			this.sound.sounds.damage2.play();
		}else{
			this.sound.sounds.damage2.stop();
		}
		this.bController.step();
		this.player.step();
		if(this.touchHandler.l.length == 0) this.player.spaceOut(-5);
		if(this.time >= this.ticksBeforeMoreChallenging){
			this.time = 0;
			this.incChallenge();
		}
		this.time++;
		if(!this.wasHit)
			this.score++;
	}else if(this.state == 2){ //tutorial
		this.wasHit = this.bController.colission();
		if(this.wasHit){
			this.health -= 2;
			this.sound.sounds.damage2.play();
		}else this.sound.sounds.damage2.stop();
		if(this.health < 0) this.health = this.maxHealth;
		this.bController.step();
		this.player.step();
		if(this.touchHandler.l.length == 0) this.player.spaceOut(-5);
		if(this.outBoundMessages.length == 0 && this.messages.length == 0){
			if(this.player.N < 2){
				this.player.setN(2);
				this.addMessage('drag your finger vertically', 'this is magetic movement', 'combine the two', '...and avoid the barriers');
				this.addMessage('the green bar at the top...', 'that is your health', 'you can\'t die in here');
				this.addMessage('keep practicing if you like', 'hit return when you are ready');
			}
		}
	}
	this.stepMessages();
};

Game.prototype.touch = function(touch, type){
	if(touch !== undefined){
		var ws = this.windowHandler.getScreenSize();
		var offset = new LV2(0, -this.windowHandler.adHeight);
		this.currentTouch = this.touchHandler.cvtToScreenLocation(touch.current.add(offset), this.gui.w, this.gui.h, ws.width, ws.height);
		if(this.state == 0) this.touchTitleScreen(type);
		else if(this.state == 1) this.touchMainGame(type);
		else if(this.state == 2) this.touchTutorial(type);
		else if(this.state == 3) this.touchPauseScreen(type);
		else if(this.state == 4) this.touchScoresScreen(type);
		else if(this.state == 6) this.touchGameOverScreen(type);
	}
};

Game.prototype.touchGameOverScreen = function(type){
	if(type == 'start' || type == 'move'){
		this.gui.goButton.touchStartMove(this.currentTouch);
		this.gui.returnButton.touchStartMove(this.currentTouch);
		this.gui.spaceButton.touchStartMove(this.currentTouch);
		this.gui.backSpaceButton.touchStartMove(this.currentTouch);
		for(var i = 0; i < this.gui.keyBoard.length; i++) this.gui.keyBoard[i].touchStartMove(this.currentTouch);
	}else if(type == 'end'){
		var txt = this.gui.nameEnterButton.text;
		if(this.gui.spaceButton.touchEnd(this.currentTouch)){
			if(txt == 'enter_name'){
				txt = ' ';
			}else if(txt.length < 12){
				txt += ' '; 
			}
		}
		for(var i = 0; i < this.gui.keyBoard.length; i++){
			if(this.gui.keyBoard[i].touchEnd(this.currentTouch)){
				if(txt == 'enter_name'){
					txt = this.gui.keyBoard[i].text;
				}else if(txt.length < 12){
					txt += this.gui.keyBoard[i].text; 
				}				
			}
		}
		if(this.gui.backSpaceButton.touchEnd(this.currentTouch))
			txt = txt.substring(0, txt.length-1);
		this.gui.nameEnterButton.text = txt;
		if(this.gui.goButton.touchEnd(this.currentTouch)){
			//save score
			this.db.addScore(this.gui.nameEnterButton.text, this.score);
			this.loadScores();
			this.stateChange(4);
			this.previousState = 0;
		}
		if(this.gui.returnButton.touchEnd(this.currentTouch)) this.stateChange(0);
	}
};

Game.prototype.touchTitleScreen = function(type){
	if(type == 'start' || type == 'move'){
		this.gui.playButton.touchStartMove(this.currentTouch);
		this.gui.tutorialButton.touchStartMove(this.currentTouch);
		this.gui.soundToggle.touchStartMove(this.currentTouch);
		this.gui.scoresButton.touchStartMove(this.currentTouch);
		this.gui.exitButton.touchStartMove(this.currentTouch);
	}else if(type == 'end'){
		if(this.gui.playButton.touchEnd(this.currentTouch)){
			this.mainGameInit();
			this.stateChange(1);
		}else if(this.gui.soundToggle.touchEnd(this.currentTouch))
			this.toggleSound();
		else if(this.gui.tutorialButton.touchEnd(this.currentTouch)){
			this.tutorialInit();
			this.stateChange(2);
		}else if(this.gui.exitButton.touchEnd(this.currentTouch)){
			if(navigator.app !== undefined){
				if(navigator.app.exitApp !== undefined) navigator.app.exitApp();
			}
		}else if(this.gui.scoresButton.touchEnd(this.currentTouch)){
			this.loadScores();
			this.stateChange(4);
		}
	}
};

Game.prototype.touchPauseScreen = function(type){
	if(type == 'start' || type == 'move'){
		this.gui.resumeButton.touchStartMove(this.currentTouch);
		this.gui.tutorialButton.touchStartMove(this.currentTouch);
		this.gui.soundToggle.touchStartMove(this.currentTouch);
		this.gui.scoresButton.touchStartMove(this.currentTouch);
		this.gui.exitButton.touchStartMove(this.currentTouch);
	}else if(type == 'end'){
		if(this.gui.resumeButton.touchEnd(this.currentTouch)){
			this.state = 1;
		}else if(this.gui.exitButton.touchEnd(this.currentTouch)){
			this.stateChange(0);
		}else if(this.gui.soundToggle.touchEnd(this.currentTouch))
			this.toggleSound();
		else if(this.gui.tutorialButton.touchEnd(this.currentTouch)){
			this.tutorialInit();
			this.stateChange(2);
		}else if(this.gui.scoresButton.touchEnd(this.currentTouch)){
			this.loadScores();
			this.stateChange(4);
		}
	}
};

Game.prototype.updatePlayer = function(){
	this.player.setCenter(this.currentTouch.x);
	var verticalDistance = Math.abs(this.currentTouch.y - this.player.y);
	verticalDistance = Math.max(verticalDistance - this.spatialSupervisor.radius*3, 0);
	this.player.setSpacing(verticalDistance*this.player.uiScalar());
};

Game.prototype.touchMainGame = function(type){
	if(type == 'start' || type == 'move'){
		if(this.gui.pauseButton.touchStartMove(this.currentTouch)) return;
		this.updatePlayer();
	}else if(type == 'end'){
		if(this.gui.pauseButton.touchEnd(this.currentTouch)){
			this.sound.sounds.damage2.stop();
			this.regularStateChange(3);
		}
	}
};

Game.prototype.touchScoresScreen = function(type){
	if(type == 'start' || type == 'move'){
		this.gui.returnButton.touchStartMove(this.currentTouch);
	}else if(type == 'end'){
		if(this.gui.returnButton.touchEnd(this.currentTouch))
			this.stateChange(this.previousState);
	}
};

Game.prototype.touchTutorial = function(type){
	if(type == 'start' || type == 'move'){
		if(this.gui.returnButton.touchStartMove(this.currentTouch)) return;
		this.updatePlayer();
	}else if(type == 'end'){
		if(this.gui.returnButton.touchEnd(this.currentTouch)){
			this.stateChange(0);
			this.sound.sounds.damage2.stop();
		}
	}
};


Game.prototype.stateChange = function(newState){
	this.previousState = this.state;
	this.transitioner.start(newState);
	this.state = 5;
};

Game.prototype.regularStateChange = function(newState){
	this.previousState = this.state;
	this.state = newState;
};

Game.prototype.drawTest = function(){
	this.gui.clear();
	this.gui.drawStars(this.starController);
	this.gui.drawPlayer(this.player, this.wasHit);
	this.gui.drawTitle('Pluto');
	this.gui.drawStartButton();
	this.gui.drawHowToPlay();
	this.gui.drawBarriers(this.bController);

	//draw currentTouch
	this.gui.debugPoint(this.currentTouch);
};


