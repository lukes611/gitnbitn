//creates a new player
//requires SpatialSupervisor
//has N : number of balls
//center : location of center ball
//y : location of balls on y axis
//spacing : the current spacing being used
//locations of balls (x-axis only)
function Player(ss){
	this.ss = ss;
	this.N = 3;
	this.center = Math.round(this.ss.width * 0.5);
	this.y = Math.round((5/8) * this.ss.height);
	this.spacing = 0;
	this.locations = undefined;
	this.rotation = 0;
	this.computeLocations();
	this.computeUIScalingScalars();
	this.computeRotationOffsets();
}

//recompute the balls locations
Player.prototype.computeLocations = function(){
	this.locations = this.ss.computeLocations(this.N, this.center, this.spacing);
};

//move the balls along x-axis by amount (positive or negative)
Player.prototype.move = function(amount){
	var cc = this.center + amount;
	if(!this.ss.playerWithinBounds(this.N, cc, this.spacing)) return;
	this.center = cc;
	this.computeLocations();
};
//set the center location for the balls
Player.prototype.setCenter = function(x){
	if(!this.ss.playerWithinBounds(this.N, x, this.spacing)) return;
	this.center = x;
	this.computeLocations();
};
//increment the spacing parameter by amount, (positive or negative)
Player.prototype.spaceOut = function(amount){
	var newSpacing = this.spacing + amount;
	if(newSpacing < 0 || !this.ss.playerWithinBounds(this.N, this.center, newSpacing)) return;
	this.spacing = newSpacing;
	this.computeLocations();
};

//set the spacing parameter
Player.prototype.setSpacing = function(x){
	if(x < 0 || !this.ss.playerWithinBounds(this.N, this.center, x)) return;
	this.spacing = x;
	this.computeLocations();
};

Player.prototype.twerpSpacing = function(x, speed){
	if(x == this.spacing) return;
	if(Math.abs(x-this.spacing) <= speed) this.setSpacing(x);
	if(x < this.spacing) this.setSpacing(this.spacing - speed);
	else this.setSpacing(this.spacing + speed);

};

//increment N by 1, max is 5
Player.prototype.incrementN = function(){
	if(this.N >= 4) return;
	this.N++;
	this.computeLocations();
};

//decrement N by 1, min is 1
Player.prototype.decrementN = function(){
	if(this.N <= 1) return;
	this.N--;
	this.computeLocations();
};

//sets N
Player.prototype.setN = function(N){
	if(N < 1 || N >= 5) return;
	this.N = N;
	this.computeLocations();
};

//computes the UI scaling factors
Player.prototype.computeUIScalingScalars = function(){
	this.uiScalars = [0];
	var distToBottom = this.ss.height - this.y;
	distToBottom *= 0.70;
	for(var i = 1; i <= 5; i++){
		this.uiScalars.push(this.ss.maximumAmountOfSpacing(i) / distToBottom);
	}
};

//returns the ui scalar for multiplying by the distance from the balls along y axis used to control the scaling
Player.prototype.uiScalar = function(){
	return this.uiScalars[this.N];
};

//compute the rotation offsets:
Player.prototype.computeRotationOffsets = function(){
	this.rotationOffsets = [];
	for(var i = 0; i <= 5; i++){
		this.rotationOffsets.push(Math.random() * 360);
	}
};

Player.prototype.step = function(){
	this.rotation += 2;
	this.rotation = this.rotation > 360 ? this.rotation - 360 : this.rotation;
};