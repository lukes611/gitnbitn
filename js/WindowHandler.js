
//an object whic handles setting the canvas to the appropriate screen size
//input is an LCanvas Object for which to do the drawing
function WindowHandler(canvas){
	this.adHeight = 50;
	this.gameTop = this.adHeight;
	this.adTop = 0;
	this.init();
	this.canvas = canvas;
	var me = this;
	window.onresize = function(){
		me.init();
		me.update();
	};
	this.repaint = window.requestAnimationFrame !== undefined ? 
	window.requestAnimationFrame.bind(window) : function(f){setTimeout(f, 30);};
	this.repaint = function(f){setTimeout(f, 33);};
}

//sets the width and height of the screen
WindowHandler.prototype.getWidthHeight = function(){
	var w = window.innerWidth;
	var h = window.innerHeight;
	if(w === undefined || h === undefined){
		w = document.documentElement.clientWidth;
		h = document.documentElement.clientHeight;
	}
	if(w === undefined || h === undefined){
		w = $(window).width();
		h = $(window).height();
	}
	this.width = w;
	this.gameHeight = h - this.adHeight;
	this.height = h - this.adHeight;
};

//initializes the object
WindowHandler.prototype.init = function(){
	this.getWidthHeight();
	this.gameTop = this.adHeight;
	this.adTop = 0;
	this.canvasId = '#canvv';
};

//returns the minimum value of this.width and this.gameHeight
WindowHandler.prototype.min = function(){
	return Math.min(this.width, this.gameHeight);
};

//returns the maxiimum value of this.width and this.gameHeight
WindowHandler.prototype.max = function(){
	return Math.max(this.width, this.gameHeight);
};

//returns the screen size for mobiles
WindowHandler.prototype.getScreenSize = function(){
	return {
		width : this.min(),
		height : this.max()
	};
};

//refreshes the screen size, sets the canvas to the screen size
WindowHandler.prototype.update = function(x){
	$(this.canvas.canvas).css('width',	this.min()+'px');
	$(this.canvas.canvas).css('height',	this.max()+'px');
	$(this.canvas.canvas).css('top', this.gameTop+'px');
	var me = this;
	if(x < 3 || x === undefined){
		var x = x === undefined? 1 : x;
		setTimeout(function(){
			me.update(x+1)
		}, 50);
	}
};