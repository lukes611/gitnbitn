//An object for breaking up space for use

//takes the width of the screen in pixels as input
function SpatialSupervisor(widthOfScreen, heightOfScreen, radius){
	this.width = widthOfScreen;
	this.height = heightOfScreen;
	this.radius = radius;
}

//returns the diameter of the radius
SpatialSupervisor.prototype.getDiameter = function(){
	return this.radius * 2;
};

//returns the total number of spaces required, given the number of balls
SpatialSupervisor.prototype.getSpacesCount = function(N){
	return Math.max(0, N-1);
};


//returns the spacing requires by the balls, given N, the number of balls
SpatialSupervisor.prototype.spaceRequiredByBalls = function(N){
	return this.getDiameter() * N;
};

//returns the maximum amount of spacing which can be given, given the number of balls, N
SpatialSupervisor.prototype.maximumAmountOfSpacing = function(N){
	return (this.width - this.spaceRequiredByBalls(N+1)) / Math.max(1, this.getSpacesCount(N));
};

//returns a random amount of spacing possible to use given the number of balls N
SpatialSupervisor.prototype.randomSpacing = function(N){
	return Math.round(this.maximumAmountOfSpacing(N) * Math.random());
};

//returns the total width of the balls together with the spacing, given the spacing size and the number of balls N
SpatialSupervisor.prototype.totalWidthOfObjects = function(N, spacing){
	return this.getDiameter() * N + this.getSpacesCount(N) * spacing;
};

//returns the left over space of the screen excluding the width of the balls, given the total number of balls and spacing
SpatialSupervisor.prototype.leftOverSpace = function(N, spacing){
	return this.width - this.totalWidthOfObjects(N, spacing);
};

//returns a random central position, given the number of balls N and the spacing 
SpatialSupervisor.prototype.getRandomCentralPosition = function(N, spacing){
	var halfSpaceOfObjects = this.totalWidthOfObjects(N, spacing) * 0.5;
	var minimumCenter = halfSpaceOfObjects + this.getDiameter();
	var maximumCenter = this.width - minimumCenter;
	return minimumCenter + Math.round(Math.random() * (maximumCenter - minimumCenter));
};

//returns a random allowable spacing and central position
SpatialSupervisor.prototype.getRandomSafeArea = function(N){
	var randomSpacing = this.randomSpacing(N);
	var pos = this.getRandomCentralPosition(N, randomSpacing);
	return {
		spacing : randomSpacing,
		position : pos
	};
};

//returns the locations of balls or safety areas given: N (number of balls), center (the centeral locations), 
SpatialSupervisor.prototype.computeLocations = function(N, center, spacing){
	var rv = [];
	var diameter = this.radius * 2;
	var isOddNumber = N % 2 == 1;
	var start = 0;
	var incrementor = diameter + spacing;
	var count = Math.floor(N / 2);
	if(isOddNumber) rv.push(center);
	start += !isOddNumber ? this.radius + spacing * 0.5 : incrementor;
	for(var i = 0; i < count; i++){
		rv.push(center + start);
		rv.push(center - start);
		start += incrementor;
	}
	return rv;
};

//returns some random safe locations, given the number of balls, N
SpatialSupervisor.prototype.randomSafeLocations = function(N){
	var safeArea = this.getRandomSafeArea(N);
	return this.computeLocations(N, safeArea.position, safeArea.spacing);
};

//takes input: an array, and removes duplicates, input has the side-effect it is sorted
SpatialSupervisor.prototype.toSet = function(input){
	if(input.length == 0) return;
	input.sort(function(a,b){return a-b;});
	var rv = [input[0]], prev = input[0];
	for(var i = 1; i < input.length; i++){
		if(input[i] != prev){
			prev = input[i];
			rv.push(prev);
		}
	}
	return rv;
};

//compute block widths, returns the rectangles as {x : number, width : number}
SpatialSupervisor.prototype.computeRects = function(locs, spacing){
	spacing = spacing === undefined ? 0 : spacing;
	var rv = [];
	locs = locs.map(function(x){return x;});
	var space = this.radius + spacing;
	var finalPos = this.width+space;
	locs.push(finalPos);
	locs = locs.sort(function(a,b){return a-b;});
	locs = locs.reduce(function(p,c){
		if(p === undefined) return [c];
		if(p[p.length-1] != c) p.push(c);
		return p;
	}, undefined);
	
	var index = 0;
	for(var i = 0; i < locs.length; i++){
		var l = locs[i];
		if((l - space)-index > 1) rv.push({x : index+0, width : (l-space)-index});
		index = l + space;
	}
	return rv;
};


//an optimized version of compute rects,
//locs is modified by reducing duplicates and is sorted
SpatialSupervisor.prototype.computeRectsFast = function(locs, spacing){
	var rv = [];
	var space = this.radius + spacing;
	var finalPos = this.width+space;
	locs.push(finalPos);
	locs = this.toSet(locs);
	
	var index = 0;
	for(var i = 0; i < locs.length; i++){
		var l = locs[i];
		if((l - space)-index > 1) rv.push({x : index+0, width : (l-space)-index});
		index = l + space;
	}
	return rv;
};


//returns true if there was a collition between circle : circlePos, and rect : {x, y, width, height}
SpatialSupervisor.prototype.rectCircleCollision = function(rect, circlePos){
	if(circlePos.x + this.radius < rect.x) return false;
	if(circlePos.x - this.radius > rect.x + rect.width) return false;
	if(circlePos.y - this.radius > rect.y + rect.height) return false;
	if(circlePos.y + this.radius < rect.y) return false;
	return true;
};

//returns true if there was a collition between circle : circlePos, and rects : a list of rectangles
SpatialSupervisor.prototype.rectsCircleCollision = function(rects, circlePos){
	var me = this;
	return rects.reduce(function(p, c){ return p || me.rectCircleCollision(c, circlePos); }, false);
};

//returns true if there was a collition between circles : a list of circles, and rects : a list of rectangles
SpatialSupervisor.prototype.rectsCirclesCollision = function(rects, circles){
	var me = this;
	return circles.reduce(function(p, c){ return p || me.rectsCircleCollision(rects, c); }, false);
};



//returns true if the player can fit in the space
SpatialSupervisor.prototype.playerWithinBounds = function(N, center, spacing){
	var halfTotalSize = this.totalWidthOfObjects(N, spacing) * 0.5;
	if(center < halfTotalSize || center > this.width - halfTotalSize) return false;
	return true;
};

