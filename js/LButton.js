
function LButton(text, fontSize, x, y, outline){
	this.x = x;
	this.y = y;
	this.fontSize = fontSize;
	this.text = text;
	this.drawOutline = outline;
	this.w = this.text.length * fontSize * 0.9;
	this.h = fontSize * 1.54;
	this.tx = this.x - this.w / 2;
	this.ty = this.y - this.h * 0.75;
	this.color = 'white';	
}

LButton.prototype.touchStartMove = function(point){
	if(point.x < this.tx || point.x > this.tx + this.w || point.y < this.ty || point.y > this.ty + this.h){
			this.color = 'white';
			return false;
	}
	else this.color = 'red';
	return true;
};

LButton.prototype.touchEnd = function(point){
	if(point.x < this.tx || point.x > this.tx + this.w || point.y < this.ty || point.y > this.ty + this.h) return false;
	this.color = 'white';
	return true;
};

//returns the y axis location at the bottom of the button / label
LButton.prototype.bottom = function(){
	return this.y + this.h;
};