function Transition(spatialSupervisor){
	this.ss = spatialSupervisor;
	this.numBars = 10;
	this.totalTicks = 50;
	this.time = 0;
	this.state = -1;
	this.height = this.ss.height / this.numBars;
}

Transition.prototype.start = function(nextState){
	this.nextState = nextState;
	var newValue = function(){ return Math.floor(Math.random() * 120.0 + 80.0); };
	this.colors = Array.apply(undefined, Array(this.numBars)).map(function(){
		return 'rgb(' + newValue() + ',' + newValue() + ',' + newValue() + ')';
	});
	this.state = 0;
	this.time = 0;
};

Transition.prototype.yaxis = function(index){
	return Math.round((this.ss.height / this.numBars) * index);
};

Transition.prototype.width = function(index){
	var time = this.state == 0 ? this.time : this.totalTicks - this.time;
	index = this.state == 0 ? index : this.numBars - index;
	var wfirst = (time / this.totalTicks) * this.ss.width * 2;
	var rv = wfirst - index * this.ss.width / this.numBars;
	return Math.min(Math.max(rv, 0), this.ss.width);
};

Transition.prototype.step = function(){
	if(this.state == 0 || this.state == 1){
		this.time++;
		if(this.time > this.totalTicks){
			if(this.state == 0){ 
				this.state = 1;
				this.time = 0;
				return true;
			}
			else if(this.state == 1) this.state = -1;
			this.time = 0;
		}
	}
	return false;
};
