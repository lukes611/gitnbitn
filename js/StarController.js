/*
Star controller has:
list : a list of stars to draw
ss   : a spatial supervisor
reverse : controls whether stars are reversed
speed : controls the speed of stars
frequency : controls how frequent stars appear
*/
function StarController(ss){
	this.ss = ss;
	this.list = [];
	this.reverse = true;
	this.speed = 1;
	this.frequency = 0.01;
	this.time = 0;
	this.init();
}

//initializes a bunch of random stars
StarController.prototype.init = function(){
	for(var i = 0; i < 20; i++){
		this.list.push({
			x : Math.round(Math.random() * this.ss.width),
			y : Math.round(Math.random() * this.ss.height),
			s : Math.round(Math.random() * 3 + 1)
		});
	}
};

//adds a new random star
StarController.prototype.addTop = function(){
	this.list.push({
		x : Math.round(Math.random() * this.ss.width),
		y : 0,
		s : Math.round(Math.random() * 3 + 1)
	});
};

//adds a new random star
StarController.prototype.addBottom = function(){
	this.list.push({
		x : Math.round(Math.random() * this.ss.width),
		y : this.ss.height,
		s : Math.round(Math.random() * 3 + 1)
	});
};

//performs a step forwards for falling stars
StarController.prototype.stepForwards = function(){
	var newList = [];
	for(var i = 0; i < this.list.length; i++){
		if(this.list[i].y < this.ss.height){
			this.list[i].y += this.speed;
			newList.push(this.list[i]);
		}
	}
	this.list = newList;
};
//performs a step forwards for rising stars
StarController.prototype.stepBackwards = function(){
	var newList = [];
	for(var i = 0; i < this.list.length; i++){
		if(this.list[i].y > 0){
			this.list[i].y -= this.speed;
			newList.push(this.list[i]);
		}
	}
	this.list = newList;
};

StarController.prototype.step = function(){
	if(Math.random() < this.frequency) this.addBottom();
	if(this.time >= 2){
		if(this.reverse) this.stepBackwards();
		else this.stepForwards();
		this.time = 0;
	}
	this.time++;
};