function GameGui(resolutionInPixels) {
	if (resolutionInPixels === undefined) resolutionInPixels = 400;
	this.w = resolutionInPixels;
	this.h = Math.round(this.w * 1.6);
	this.canvas = new LCanvas(this.w, this.h);
	this.canvas.canvas.id = "canvv";
	this.radius = this.w * 0.035;
	this.time = 0;

	this.largeFontSize = Math.round(this.w * 0.09);
	this.smallFontSize = Math.round(this.w * 0.06);
	this.kbFontSize = Math.round(this.w * 0.08);
	this.hw = Math.round(this.w * 0.5);
	this.w8 = Math.round(this.w * 0.8);
	this.gapA = Math.round(this.w * 0.02);
	this.gapB = this.gapA * 4.0;


	this.initScoresLabels();
	this.initKB();

	this.title = new LButton('iSplit', this.largeFontSize, this.hw, this.hw, false);
	this.playButton = new LButton('play', this.smallFontSize, this.hw, this.w8, true);
	this.resumeButton = new LButton('resume', this.smallFontSize, this.hw, this.w8, true);
	this.messageLabel = new LButton('generic message', this.smallFontSize, this.hw, this.w8, true);
	this.tutorialButton = new LButton('tutorial', this.smallFontSize, this.hw, this.playButton.bottom() + this.gapA, true);
	this.soundToggle = new LButton('sound off', this.smallFontSize, this.hw, this.tutorialButton.bottom() + this.gapA, true);
	this.scoresButton = new LButton('scores', this.smallFontSize, this.hw, this.soundToggle.bottom() + this.gapA, true);
	this.exitButton = new LButton('exit', this.smallFontSize, this.hw, this.scoresButton.bottom() + this.gapA, true);
	this.pauseButton = new LButton('pause', this.smallFontSize, this.w - 2.5 * this.smallFontSize, this.gapB, true);
	this.returnButton = new LButton('return', this.smallFontSize, this.w - 3 * this.smallFontSize, this.gapB, true);

	this.healthWidth = this.w - this.returnButton.w - 0.2 * this.returnButton.w;
	this.healthTy = this.returnButton.ty;
	this.healthHeight = this.returnButton.h;

	this.playerScoreLabel = new LButton('score: 0', Math.round(this.kbFontSize * 0.5), this.hw, this.healthTy + this.healthHeight, false);
	this.playerScoreLabel.ty += this.playerScoreLabel.h;
	this.playerScoreLabel.y += this.playerScoreLabel.h;


}

GameGui.prototype.initScoresLabels = function () {
	var topLabelY = Math.round(this.w / 3);
	this.finalScore = new LButton('final score', this.smallFontSize, this.hw, topLabelY, false);
	this.finalScoreOut = new LButton('0', this.smallFontSize, this.hw, this.finalScore.bottom() + this.gapA, false);
	this.nameEnterButton = new LButton('enter_name', this.smallFontSize, this.hw, this.finalScoreOut.bottom() + this.gapA * 2, true);


	this.scoresLabel = new LButton('your scores', this.w * 0.08, this.w / 2, this.w / 3, false);
	this.yourScoresLabels = [new LButton('', this.smallFontSize, this.w / 2, this.scoresLabel.y + this.scoresLabel.h, false)];
	for (var i = 1; i < 5; i++) {
		this.yourScoresLabels.push(new LButton('', this.smallFontSize, this.w / 2, this.yourScoresLabels[i - 1].y + this.yourScoresLabels[i - 1].h, false));
	}
	this.scoresLabel2 = new LButton('world scores', this.w * 0.08, this.w / 2, this.w, false);
	this.worldScoresLabels = [new LButton('coming soon', this.smallFontSize, this.w / 2, this.scoresLabel2.y + this.scoresLabel2.h, false)];
	for (var i = 1; i < 5; i++) {
		this.worldScoresLabels.push(new LButton('undefined', this.smallFontSize, this.w / 2, this.worldScoresLabels[i - 1].y + this.worldScoresLabels[i - 1].h, false));
	}
};

GameGui.prototype.initKB = function () {
	this.keyBoard = [];
	var nb = undefined;

	var topY = this.nameEnterButton.bottom() + this.gapA * 6;
	var test = new LButton('A', this.kbFontSize, 0, 0, true);
	var gapC = test.w * 0.38;
	var grid = 'q,w,e,r,t,y,u,i,o,p_a,s,d,f,g,h,j,k,l_z,x,c,v,b,n,m'.split('_');
	var pos = {
		x: 0,
		y: topY
	};
	for (var j = 0; j < 3; j++) {
		var g = grid[j].split(',');
		pos.x = (test.w + gapC) / 2;
		for (var i = 0; i < g.length; i++) {
			nb = new LButton('' + g[i] + '', this.kbFontSize, pos.x, pos.y, true);
			nb.fontSize = this.kbFontSize * 0.8;
			this.keyBoard.push(nb);
			pos.x += test.w + gapC;
		}
		pos.y += test.h + gapC;
	}
	pos.y -= test.h + gapC;
	pos.x += test.w;
	nb = new LButton('bsp', this.kbFontSize, pos.x, pos.y, true);
	nb.fontSize = this.kbFontSize * 0.8;
	this.backSpaceButton = nb;
	pos.y += test.h + gapC;
	this.spaceButton = new LButton('   space   ', this.kbFontSize - 2, this.w / 2, pos.y, true);
	this.goButton = new LButton('submit', this.kbFontSize, this.w / 2, this.spaceButton.y + this.spaceButton.h + 20, true);

};


GameGui.prototype.insert = function () {
	$(document.body).append(this.canvas.canvas);
};

GameGui.prototype.drawHealth = function (percent) {
	this.canvas.setFill('green');
	this.canvas.rectf(0, this.healthTy, this.healthWidth * percent, this.healthHeight);
};

GameGui.prototype.drawPlayerCircle = function (x, y, r, a) {
	this.canvas.context.translate(x, y);
	this.canvas.context.rotate(a / 57.3);
	var topOffs = r * 0.35;
	var r2 = r * 0.6;
	this.canvas.circlef(0, 0, r);
	this.canvas.context.stroke();
	this.canvas.line(-r2, -topOffs, r2, -topOffs);
	this.canvas.line(-r2, 0, r2, 0);
	this.canvas.line(-r2, topOffs, r2, topOffs);
	this.canvas.resetTransform();
};

GameGui.prototype.drawButton = function (b) {
	this.canvas.context.font = b.fontSize + 'pt Century Gothic';
	this.canvas.context.textAlign = 'center';
	this.canvas.context.fillStyle = b.color;
	this.canvas.context.fillText(b.text, b.x, b.y);
	if (b.drawOutline) {
		this.canvas.setStroke(b.color);
		this.canvas.rect(b.tx, b.ty, b.w, b.h);
	}
};

GameGui.prototype.drawTitle = function (name) {
	this.drawButton(this.title);
};

GameGui.prototype.drawStartButton = function () {
	this.drawButton(this.resumeButton);
	this.drawButton(this.tutorialButton);
	this.drawButton(this.soundToggle);
	this.drawButton(this.exitButton);
	this.drawButton(this.scoresButton);
	this.drawButton(this.pauseButton);
	this.drawButton(this.returnButton);
};

GameGui.prototype.drawHowToPlay = function () {
	return;
	this.canvas.context.font = '20pt Century Gothic';
	this.canvas.context.textAlign = 'center';
	this.canvas.context.fillStyle = 'white';
	var msg = 'swipe left and right,is the best,in the world'.split(',');
	for (var i = 0; i < msg.length; i++)
		this.canvas.context.fillText(msg[i], this.w / 2, this.w + 50 + 30 * i);

};

GameGui.prototype.drawPlayer = function (p, wasHit) {
	this.canvas.context.lineWidth = 2;
	if (!wasHit) {
		this.canvas.setFill('rgb(11,168,105)');
		this.canvas.setStroke('white');
	} else {
		this.canvas.setFill('red');
		this.canvas.setStroke('black');
	}

	for (var i = 0; i < p.locations.length; i++)
		this.drawPlayerCircle(p.locations[i], p.y, p.ss.radius, p.rotation + p.rotationOffsets[i]);
};

GameGui.prototype.drawBarriers = function (bController) {
	for (var i = 0; i < bController.list.length; i++) {
		var barrier = bController.list[i];
		for (var j = 0; j < barrier.blocks.length; j++) {
			this.canvas.setFill(barrier.colors[j]);
			this.canvas.rectf(barrier.blocks[j].x, barrier.y, barrier.blocks[j].width, barrier.height);
		}
	}
};

GameGui.prototype.drawStars = function (starController) {
	this.canvas.setFill('white');
	for (var i = 0; i < starController.list.length; i++)
		this.canvas.rectf(starController.list[i].x, starController.list[i].y, starController.list[i].s, starController.list[i].s);
};

GameGui.prototype.debugPoint = function (point) {
	this.canvas.setFill('yellow');
	this.canvas.circlef(point.x, point.y, 5);
};

GameGui.prototype.clear = function () {
	this.canvas.setFill('rgb(31,3,33)');
	this.canvas.rectf(0, 0, this.w, this.h);
};

GameGui.prototype.overlay = function () {
	this.canvas.setFill('rgba(31,3,33,0.8)');
	this.canvas.rectf(0, 0, this.w, this.h);
};

GameGui.prototype.step = function () {
	this.time = (this.time + 1) % 2;
};