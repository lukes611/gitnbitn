//Barriers block players
//have: y (the y-axis location, height : the height of the walls, 
//blocks : the x-axes and widths, N : the number of balls which can fit through
function Barrier(y, height, blocks, N){
	this.y = y;
	this.height = height;
	this.blocks = blocks;
	this.N = N;
	this.colors = [];
	for(var i = 0; i < this.blocks.length; i++){
		var rred = Math.round(Math.random() * 120 + 80);
		var rgreen = Math.round(Math.random() * 80 + 20);
		var rblue = Math.round(Math.random() * 80 + 20);
		this.colors.push('rgb('+rred+','+rgreen+','+rblue+')');	
	} 
}

//returns true if the barrier is still alive, else returns false
//takes in a spatial supervisor
Barrier.prototype.alive = function(ss){
	if(this.y < ss.height) return true;
	return false;
};

//performs early detection of whether there is a collision, given player
Barrier.prototype.collisionY = function(player){
	if(player.y - player.ss.radius > this.y + this.height) return false;
	if(player.y + player.ss.radius < this.y) return false;
	return true;
};

//performs x-axis collision detection, given player
Barrier.prototype.collisionX = function(player){
	var collisionDetected = true, loc, rad = player.ss.radius, t1, t2;
	for(var l = 0; l < player.locations.length; l++)
	{
		loc = player.locations[l];
		for(var b = 0; b < this.blocks.length; b++){
			//tests
			t2 = loc - rad > this.blocks[b].x + this.blocks[b].width;
			if(t2) continue;
			t1 = loc + rad < this.blocks[b].x;
			if(!t1 && !t2) return true;
		}	
	}
	return false;
};

//returns true if there was a collision between a player and the barrier
Barrier.prototype.collision = function(player){
	if(!this.collisionY(player)) return false;
	return this.collisionX(player);
};