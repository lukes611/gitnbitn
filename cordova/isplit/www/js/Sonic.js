
//LSound is an object which represents an audio object
function LSound(name, type){
	if(type === undefined) type = 'ogg';
	this.name = name;
	this.type = type;
	this.src = './sounds/' + name + '.' + this.type;
	this.a = new Audio();
	this.a.setAttribute('src', this.src);
	this.isPlaying = false;
	this.a.loop = true;
	this.volume = 1.0;
}

//plays the sound, if the sound is already playing: has no effect
LSound.prototype.play = function(){
	try{
		if(!this.isPlaying)
			this.a.play();
		this.isPlaying = true;
	}catch(err){}
};

//stops the sound from playing, next time play is called, sound starts from the start
LSound.prototype.stop = function(){
	try{
		this.a.pause();
		this.a.currentTime = 0;
		this.isPlaying = false;
	}catch(err){
		
	}
};

//mutes the sound
LSound.prototype.mute = function(){
	this.a.volume = 0;
};

//sets the volume for the sound
LSound.prototype.setVolume = function(vs){
	try{
		this.volume = vs;
		this.a.volume = vs;
	}catch(err){}
};

//unmutes the sound
LSound.prototype.unmute = function(){
	try{
		this.a.volume = this.volume;
	}catch(err){}
};

//an object which holds sounds
function Sonic(){
	this.sounds = {};
	for(var i = 0; i < arguments.length; i++){
		this.sounds[arguments[i]] = new LSound(arguments[i]);
	}
}

//mutes all the sounds
Sonic.prototype.muteAll = function(){
	for(var sound in this.sounds){
		this.sounds[sound].mute();
	}
};

//unmutes all the sounds
Sonic.prototype.unmuteAll = function(){
	for(var sound in this.sounds){
		this.sounds[sound].unmute();
	}
};

//returns a sound by it's name
Sonic.prototype.at = function(name){
	return this.sounds[name];
};