
//database object handles both 
function DBObject(){
	this.scores = [];
	this.init();
}

DBObject.prototype.init = function(){
	var st = localStorage.getItem('localScores');
	if(st === '' || st === undefined || st === null) this.scores = [];
	else this.scores = JSON.parse(st);
};

/*
for tommi todo: access google play and get the top5WorldScores, returns a list of objects like the one below
*/
DBObject.prototype.getTop5WorldScores = function(){
	
};

//for tommi: save the name and score to google play
DBObject.prototype.addWorldScore = function(name, score){

};


DBObject.prototype.addScore = function(name, score){
	this.scores.push({name: name, score: score});
	this.scores.sort(function(a,b){return b.score - a.score; });
	this.scores = this.scores.slice(0,5);
	localStorage.setItem('localScores', JSON.stringify(this.scores));
	this.addWorldScore(name, score);
};

DBObject.prototype.getTop5UserScores = function(){
	return this.scores;
};




