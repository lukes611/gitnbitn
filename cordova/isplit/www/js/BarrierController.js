/*Barrier controller has:
list : a list of barriers,
player : access to the player,
ss : a spatial supervisor,
time : a counter,
speed : the number of counts before a new barrier is added
*/
function BarrierController(ss, player, speed){
	this.ss = ss;
	this.player = player;
	this.list = [];
	this.time = speed - 1;
	this.speed = speed;
	this.yspeed = 1;
	this.barrierHeight = this.ss.height * 0.05; //was 20
	this.freeze = false;
	this.newN = 1;
	this.extraSpace = 1.0;
}


//creates a new randomized barrier
BarrierController.prototype.createBarrier = function(){
	var safes = this.ss.randomSafeLocations(this.player.N);
	var rects = this.ss.computeRectsFast(safes, (Math.random() * this.ss.getDiameter() + this.ss.radius) * this.extraSpace); //was random * 15 + 10
	return new Barrier(0, this.barrierHeight, rects, this.player.N);
};

//resets the state to the beginning
BarrierController.prototype.reset = function(){
	this.time = this.speed - 1;
	this.freeze = false;
	this.list = [];
};

//adds a new barrier
BarrierController.prototype.add = function(){
	this.list.push(this.createBarrier());
};

//detects whether a collision occurred
BarrierController.prototype.colission = function(){
	for(var i = 0; i < this.list.length; i++)
		if(this.list[i].collision(this.player)) return true;
	return false;
};

//take 1 time step
BarrierController.prototype.step = function(){
	
	if(this.time >= this.speed){
		this.time = 0;
		if(!this.freeze)
			this.add();
		else if(this.list.length == 0){
			this.player.setN(this.newN);
			this.freeze = false;
		}
	}

	var newList = [];
	for(var i = 0; i < this.list.length; i++){
		if(this.list[i].alive(this.ss)){
			this.list[i].y += this.yspeed;
			newList.push(this.list[i]);
		}
	}

	this.list = newList;
	
	this.time++;
};

BarrierController.prototype.incrementN = function(){
	this.newN = this.player.N+1;
	this.freeze = true;
};

BarrierController.prototype.decrementN = function(){
	this.newN = this.player.N-1;
	this.freeze = true;
};

BarrierController.prototype.setN = function(N){
	this.newN = N;
	this.freeze = true;
};

